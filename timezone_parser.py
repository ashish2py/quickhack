'''
Timezone parser : Provide latitude and longitude to get 'Timezone'
'''

import urllib2
import requests
from lxml import etree
from lxml.etree import fromstring

def timezone_parser(lat,lng):    
    
    try:
        url = 'http://www.earthtools.org/timezone/'+str(lat)+'/'+str(lng)
        request = urllib2.urlopen(url)
        r_code = request.getcode()      #response code
            
        #content
        content = request.read()
        
        #unicode parsing
        xml = content.encode('utf-8')
        parser = etree.XMLParser(ns_clean=True, recover=True, encoding='utf-8')
        content_parsed = fromstring(xml, parser=parser)
        
        #get value from reponse
        offset  = content_parsed.findtext('offset')
        offset_label = str(offset)+' hours'
        minutes = int(offset)*60
        
        #print lat,lng,offset_label,minutes
        
        return offset_label,minutes
    except Exception as e:
        print e,url,offset_label,minutes

if __name__ == '__main__':
    timezone_parser(lat, lng)




"""
   
   --- calling ---

   import timezone_parser


   lat = 5.417920
   lng = 100.329597

   offset_label,minutes = timezone_parser(lat,lng)


"""